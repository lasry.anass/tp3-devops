package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {
	public int x;
	private int y;
	public int getX(){
		return y;
	}
	@GetMapping("/")
	String home() {
		return "Spring is here!";
	}

	@GetMapping("/coucou")
	String coucou() {
		return "Coucou!";
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}